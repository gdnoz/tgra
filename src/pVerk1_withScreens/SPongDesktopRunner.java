package pVerk1_withScreens;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class SPongDesktopRunner {
	public static void main(String[] args) {
		new LwjglApplication(new SPong(), "SPong!", 800, 600, false);
	}
}
