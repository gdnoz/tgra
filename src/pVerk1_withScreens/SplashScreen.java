package pVerk1_withScreens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SplashScreen extends AbstractScreen {
	
	private Texture splashTexture;
	private TextureRegion splashTextureRegion;
	
	// The y position of the splash texture
	private float yPos;
	private float xPos;
	
	// Menu strings
	private String[] menu;
	
	public SplashScreen( SPong game ) {
		super( game );
		yPos = 700;
		xPos = 860;
		font.setColor( .7f, 0, 0, .8f );
		font.setScale(1.5f);
		menu = new String[] {"Press spacebar to continue . . .", "If you dare . . ."};
	}

	@Override
	public void dispose() {
		super.dispose();
		splashTexture.dispose();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		super.show();
		
		splashTexture = new Texture( "img/pong.png" );
		splashTexture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		splashTextureRegion = new TextureRegion( splashTexture, 100, -50, 860, 700);
	}
	
	@Override
	public void render( float delta ) {
		super.render(delta);

		batch.begin();
		batch.draw( splashTextureRegion, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight() );
		conditionalDraw();
		batch.end();
	}
	
	private void conditionalDraw() {
		if (xPos < 1290 && yPos < 1050) {
			++xPos;
			yPos += .8139f;
			
			if (Gdx.input.isKeyPressed(Keys.ENTER) || Gdx.input.isKeyPressed(Keys.ESCAPE)) {
				xPos = 1290;
				yPos = 1050;
			}
			
			splashTextureRegion.setRegionWidth((int)xPos);
			splashTextureRegion.setRegionHeight((int)yPos);
		}
		else {
			for (int i = 0; i < menu.length; i++) {
				this.font.draw(
						batch,
						menu[i],
						Gdx.graphics.getWidth()/2 - font.getBounds(menu[i]).width/2,
						(Gdx.graphics.getHeight()/2 + font.getBounds(menu[i]).height*2)-i*(font.getBounds(menu[i]).height+12));
			}
			
			if (Gdx.input.isKeyPressed(Keys.SPACE))
				game.setScreen(new GameScreen( game ));
		}
	}

}
