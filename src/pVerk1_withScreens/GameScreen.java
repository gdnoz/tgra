package pVerk1_withScreens;

import java.nio.FloatBuffer;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.BufferUtils;

public class GameScreen extends AbstractScreen {
	
	// Game constants
	private float BALL_RADIUS = 10;
	private float PADDLE_WIDTH = BALL_RADIUS;
	private float PADDLE_HEIGHT = 140;
	
	private FloatBuffer vertexBuffer;
	private float paddleWidth = 12f;
	private float paddleHeight = 120f;
	
	// Stuff for text
	private SpriteBatch spriteBatch;
	private BitmapFont font;
	
	// Randy, the random variable... Because you never know.
	Random randy = new Random();
	
	// Ball velocity vector
	private Vector2 velocity;
	
	// Our model objects
	private Vector2 hPad;
	private Vector2 cPad;
	private Vector2 ball;
	
	// The player scores
	private int hScore;
	private int cScore;
	
	// Initialization function. Called in the beginning and when a player scores
	private void initialize(int p) {
		// Initialize ball position
		this.ball = new Vector2(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
		
		// Initialize the ball velocity vector, and set a random angle
		// within 45 degrees of true west (towards the human paddle)
		this.velocity = new Vector2(4f, 0);
		this.velocity.setAngle(randy.nextFloat()*90 + p*180 - 45);
	}

	public GameScreen(SPong game) {
		super(game);
		// Run the reusable initialize function
		initialize(1);
		
		// Initialize the paddle positions
		this.hPad = new Vector2(this.PADDLE_WIDTH, Gdx.graphics.getHeight()/2 - this.PADDLE_HEIGHT/2);
		this.cPad = new Vector2(Gdx.graphics.getWidth() - this.PADDLE_WIDTH*2, Gdx.graphics.getHeight()/2 - this.PADDLE_HEIGHT/2);
		
		// Initialize the player scores
		this.hScore = this.cScore = 0;
		
		// Initialize text stuff
		this.spriteBatch = new SpriteBatch();
		this.font = new BitmapFont();
		this.font.setScale(2.5f);
		this.font.setColor(0, .5f, 1f, 1);
		
		// Enable vertex arrays
		Gdx.gl11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		
		// Specify the background color of the window
		Gdx.gl11.glClearColor(0, 0, 0, 1);
		
		// Create float buffers for the paddles and ball respectively
		// and then shoving them into the same buffer
		this.vertexBuffer = BufferUtils.newFloatBuffer(652);
		float[] floatBuf = new float[652];
		float[] tempBuf = new float[] {
				0,					0,
				0,					this.PADDLE_HEIGHT,
				this.PADDLE_WIDTH,	0,
				this.PADDLE_WIDTH,	this.PADDLE_HEIGHT};
		for (int i = 0; i < 8; i++) {
			floatBuf[i] = tempBuf[i];
		}
		
		double pi = Math.PI;
		int s = 8;
		for (double i = 0; i<2*pi; i += pi/120){
			floatBuf[s++] = (float)Math.cos(i);
			floatBuf[s++] = (float)Math.sin(i);
		}
		this.vertexBuffer.put(floatBuf);
		this.vertexBuffer.rewind();
	}
	
	@Override
	public void dispose() {
		super.dispose();
		
		spriteBatch.dispose();
		font.dispose();
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// Load the Project matrix. Next commands will be applied on that matrix.
		Gdx.gl11.glMatrixMode(GL11.GL_PROJECTION);
		Gdx.gl11.glLoadIdentity();
		
		// Set up a two-dimensional orthographic viewing region.
		Gdx.glu.gluOrtho2D(Gdx.gl10, 0, width, 0, height);

		// Set up affine transformation of x and y from world coordinates to window coordinates
		Gdx.gl11.glViewport(0, 0, width, height);
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void render( float delta ) {
		display();
		update();
	}
	
	private void display() {
		// Clear the screen
		Gdx.gl11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		// Enter ModelView matrix mode
		Gdx.gl11.glMatrixMode(GL11.GL_MODELVIEW);

		// Reset the ModelView matrix
		Gdx.gl11.glLoadIdentity();
		
		Gdx.gl11.glVertexPointer(2, GL11.GL_FLOAT, 0, this.vertexBuffer);
		
		// Draw the human pad
		Gdx.gl11.glPushMatrix();
		Gdx.gl11.glTranslatef(hPad.x, hPad.y, 0);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		Gdx.gl11.glPopMatrix();
		
		// Draw the computer pad
		Gdx.gl11.glPushMatrix();
		Gdx.gl11.glTranslatef(cPad.x, cPad.y, 0);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		Gdx.gl11.glPopMatrix();
		
		// Draw the ball
		Gdx.gl11.glPushMatrix();
		Gdx.gl11.glTranslatef(ball.x, ball.y, 0);
		Gdx.gl11.glScalef(BALL_RADIUS, BALL_RADIUS, BALL_RADIUS);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_FAN, 8, 300);
		Gdx.gl11.glPopMatrix();
		
		// Draw the current score
		String hScoreStr = String.format("%d", hScore);
		String cScoreStr = String.format("%d", cScore);
		spriteBatch.begin();
		font.draw(
				spriteBatch,
				hScoreStr,
				Gdx.graphics.getWidth()/2 - font.getBounds(hScoreStr).width - 25,
				590);
		font.draw(
				spriteBatch,
				cScoreStr,
				Gdx.graphics.getWidth()/2 + 25,
				590);
		spriteBatch.end();
	}
	
	private void update() {
		lControl(hPad);
		rControl(cPad);
		checkForWin();
		checkForCollision();
		if (ball.tmp().add(velocity).y < 0)
			ball.add(velocity.tmp().mul(ball.y/velocity.y));
		else if (ball.tmp().add(velocity).y > Gdx.graphics.getHeight())
			ball.add(velocity.tmp().mul(velocity.y/ball.y));
		else
			ball.add(velocity);
	}
	
	private void rControl(Vector2 v) {
		float t = 7;
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			if (v.y + PADDLE_HEIGHT + t > Gdx.graphics.getHeight()) {
				t = Gdx.graphics.getHeight() - (v.y + PADDLE_HEIGHT);
			}
			v.add(0, t);
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			if (v.y - t < 0) {
				t = v.y;
			}
			v.sub(0, t);
		}
	}
	private void lControl(Vector2 v) {
		float t = 7;
		if (Gdx.input.isKeyPressed(Keys.W)) {
			if (v.y + PADDLE_HEIGHT + t > Gdx.graphics.getHeight()) {
				t = Gdx.graphics.getHeight() - (v.y + PADDLE_HEIGHT);
			}
			v.add(0, t);
		}
		if (Gdx.input.isKeyPressed(Keys.S)) {
			if (v.y - t < 0) {
				t = v.y;
			}
			v.sub(0, t);
		}
	}
	
	// This always wins (Only for demonstration purposes)
	// TODO: Make it fail
	private void automove(Vector2 v) {
		if (ball.y + PADDLE_HEIGHT/2 < Gdx.graphics.getHeight()
				&& ball.y - PADDLE_HEIGHT/2 > 0)
			v.y = ball.y-PADDLE_HEIGHT/2;
	}
	
	private void checkForWin() {
		if (ball.x+BALL_RADIUS < 0) {
			++cScore;
			initialize(1);
		}
		else if (ball.x-BALL_RADIUS > Gdx.graphics.getWidth()) {
			++hScore;
			initialize(0);
		}
	}
	
	private void checkForCollision() {
		// Check for floor/ceiling collision
		if (ball.y+BALL_RADIUS >= Gdx.graphics.getHeight())
			reflect(new Vector2(0, -1));
		else if (ball.y-BALL_RADIUS <= 0)
			reflect(new Vector2(0, 1));
		
		// Check for paddle collision
		if (ball.x-BALL_RADIUS <= PADDLE_WIDTH*2
				&& ball.x-BALL_RADIUS > PADDLE_WIDTH*2 - velocity.len()
				&& ball.y >= hPad.y
				&& ball.y <= hPad.y+PADDLE_HEIGHT) {
			float rotation = (ball.y - (hPad.y+(PADDLE_HEIGHT/2)))/(PADDLE_HEIGHT/2);
			velocity.setAngle(rotation * 72);
			velocity.add(velocity.tmp().nor());
		}
		else if (ball.x+this.BALL_RADIUS >= Gdx.graphics.getWidth()-PADDLE_WIDTH*2
				&& ball.x+BALL_RADIUS < Gdx.graphics.getWidth() - PADDLE_WIDTH*2 + velocity.len()
				&& ball.y >= cPad.y
				&& ball.y <= cPad.y+PADDLE_HEIGHT) {
			float rotation = (ball.y - (cPad.y+(PADDLE_HEIGHT/2)))/(PADDLE_HEIGHT/2);
			velocity.setAngle(-1 * rotation * 72 + 180);
			velocity.add(velocity.tmp().nor());
		}
	}
	
	private void reflect(Vector2 n) {
		// Apply the equation r = a - 2n( a dot n )
		float dot = velocity.tmp().dot(n);
		velocity.sub(n.tmp().mul(2).mul(dot));
	}

}
