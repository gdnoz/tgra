package dVerk1;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

public class DesktopRunner {
	public static void main(String[] args) {
		new LwjglApplication(new BoxWorld(), "Awesome!", 800, 600, false);
	}
}
