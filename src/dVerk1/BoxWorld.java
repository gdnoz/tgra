package dVerk1;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.BufferUtils;

public class BoxWorld implements ApplicationListener {
	
	private class Box {
		private float x, y;
		public Box(float x, float y) {
			this.x = x;
			this.y = y;
		}
		public float getX() { return this.x; }
		public void setX(float x) { this.x = x; }
		public float getY() { return this.y; }
		public void setY(float y) { this.y = y; }
	}
	
	// Random utility for initial main box position
	private Random random = new Random();
	
	// The groovy box
	private Box mainBox;
	
	// An array for all the boring click boxes
	private ArrayList<Box> boxes;
	
	// Width/height of every box
	private float boxWidth = 50;
	
	// Vertex buffer
	private FloatBuffer vertexBuffer;
	
	// Trajectory angle of the main box
	/*
	 * 0 - up+right
	 * 1 - down+right
	 * 2 - down+left
	 * 3 - up+left
	 */
	private int angle;
	
	// Values of red, green and blue in the color of the main box
	private float c_r = 0, c_g = 0, c_b = 0;
	// Alternating between 1 and 0, these are useful for keeping
	// track of color cycling state of the main box
	private int t_r = 0, t_g = 0, t_b = 0;
	
	// Boolean variables to decide whether to auto-move the main box
	// And display HUD
	private boolean automove;
	private boolean showHUD;
	
	// Stuff for text
	private SpriteBatch spriteBatch;
	private BitmapFont font;
	
	@Override
	public void create() {
		// Initialize text stuff
		this.spriteBatch = new SpriteBatch();
		this.font = new BitmapFont();
		
		// Initialize box position randomly... sort of
		this.mainBox = new Box(
				random.nextFloat() * (800 - this.boxWidth),
				random.nextFloat() * (600 - this.boxWidth));
		
		// Initialize an empty box array
		this.boxes = new ArrayList<Box>();
		
		// Initialize box trajectory angle and tell the main box to move
		this.angle = 0;
		this.automove = true;
		this.showHUD = false;
		
		// Enable vertex arrays
		Gdx.gl11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		
		// Specify the background color of the window
		Gdx.gl11.glClearColor(.2f, 0, .2f, 1);
		
		// Create vertex buffer for the box
		this.vertexBuffer = BufferUtils.newFloatBuffer(8);
		this.vertexBuffer.put(new float[] {0,0,0,boxWidth,boxWidth,0,boxWidth,boxWidth});
		this.vertexBuffer.rewind();
		
		// Specify the location of data to draw in the vertex buffer
		Gdx.gl11.glVertexPointer(2, GL11.GL_FLOAT, 0, this.vertexBuffer);
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}
	
	// Actual rendering of objects when and where they are
	private void display() {
		// Clear the screen
		Gdx.gl11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		// Enter ModelView matrix mode
		Gdx.gl11.glMatrixMode(GL11.GL_MODELVIEW);
		
		Gdx.gl11.glVertexPointer(2, GL11.GL_FLOAT, 0, this.vertexBuffer);
		
		// Draw all the boring little boxes in a boring color
		Gdx.gl11.glColor4f(.3f, 0, .3f, 1);
		for (Box b : this.boxes) {
			Gdx.gl11.glLoadIdentity();
			Gdx.gl11.glTranslatef(b.getX(), b.getY(), 0);
			Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		}
		
		Gdx.gl11.glLoadIdentity();
		
		// Apply translation to the ModelView matrix for main box
		Gdx.gl11.glTranslatef(this.mainBox.getX(), this.mainBox.getY(), 0);
				
		// Set the color of the main box and draw!
		if (checkBoxCollision())
			Gdx.gl11.glColor4f(1, 0, 0, 0);
		else
			Gdx.gl11.glColor4f(this.c_r, this.c_g, this.c_b, 0);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		
		// Write info to the screen in some mean-lookin' green
		if (this.showHUD){ 
			this.spriteBatch.begin();
			font.setColor(0, .8f, 0, 1);
			font.draw(this.spriteBatch, String.format("R: %.2f", this.c_r), 10, 590);
			font.draw(this.spriteBatch, String.format("G: %.2f", this.c_g), 10, 575);
			font.draw(this.spriteBatch, String.format("B: %.2f", this.c_b), 10, 560);
			font.draw(this.spriteBatch, String.format("Box at: (%.0f, %.0f)",
					this.mainBox.getX(), this.mainBox.getY()), 10, 20);
			font.draw(this.spriteBatch, String.format("Cursor at: (%d, %d)", Gdx.input.getX(), Gdx.graphics.getHeight()-Gdx.input.getY()), 660, 20);
			font.draw(this.spriteBatch, String.format("%d FPS", Gdx.graphics.getFramesPerSecond()), 745, 590);
			this.spriteBatch.end();
		}
	}
	
	// Updates to object states
	private void update() {
		updateColor();
		
		// If the mouse is clicked on the screen, Add a new box to the array (as long as it does not overlap the main box)
		if (Gdx.input.justTouched() 
				&& ((600-Gdx.input.getY() < this.mainBox.getY()-(this.boxWidth/2)
						|| 600-Gdx.input.getY() > this.mainBox.getY()+(this.boxWidth*1.5))
						|| (Gdx.input.getX() < this.mainBox.getX()-(this.boxWidth/2)
								|| Gdx.input.getX() > this.mainBox.getX()+(this.boxWidth*1.5)))) {
			this.boxes.add(new Box(
					Gdx.input.getX() - (this.boxWidth/2),
					Gdx.graphics.getHeight() - Gdx.input.getY() - (this.boxWidth/2)));
		}
		
		// If spacebar is pressed, switch to manual movement
		// Else if Esc is pressed, switch back to automatic
		if (Gdx.input.isKeyPressed(Keys.SPACE))
			this.automove = false;
		else if (Gdx.input.isKeyPressed(Keys.ESCAPE))
			this.automove = true;
		
		// While x is pressed, display HUD
		if (Gdx.input.isKeyPressed(Keys.X))
			this.showHUD = true;
		else
			this.showHUD = false;
		
		// Handle moving of ze main box
		if (!automove) 
			listenKeys();
		else
			moveBox();
	}
	
	private void updateColor() {
		// Cycle the color of the box through the visible spectrum
		if (this.c_r < .991f && this.t_r == 0)
			this.c_r += .009f;
		else if (this.c_r > .009f && this.t_r == 1)
			this.c_r -= .009f;
		else
			this.t_r = ++this.t_r % 2;

		if (this.c_g < .988f && this.t_g == 0)
			this.c_g += .012f;
		else if (this.c_g > .012f && this.t_g == 1)
			this.c_g -= .012f;
		else
			this.t_g = ++this.t_g % 2;

		if (this.c_b < .985f && this.t_b == 0)
			this.c_b += .015f;
		else if (this.c_b > .015f && this.t_b == 1)
			this.c_b -= .0125;
		else
			this.t_b = ++this.t_b % 2;
	}
	
	private void listenKeys() {
		if(this.mainBox.getX() >= 0 && (Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT))){
			this.mainBox.setX(this.mainBox.getX() - 3);
			if (checkBoxCollision())
				this.mainBox.setX(this.mainBox.getX() + 3);
		}
		if(this.mainBox.getX() <= (800-this.boxWidth) && (Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT))){
			this.mainBox.setX(this.mainBox.getX() + 3);
			if (checkBoxCollision())
				this.mainBox.setX(this.mainBox.getX() - 3);
		}
		if(this.mainBox.getY() <= (600-this.boxWidth) && (Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP))){
			this.mainBox.setY(this.mainBox.getY() + 3);
			if (checkBoxCollision())
				this.mainBox.setY(this.mainBox.getY() - 3);
		}
		if(this.mainBox.getY() >= 0 && (Gdx.input.isKeyPressed(Keys.S)|| Gdx.input.isKeyPressed(Keys.DOWN))){
			this.mainBox.setY(this.mainBox.getY() - 3);
			if (checkBoxCollision())
				this.mainBox.setY(this.mainBox.getY() + 3);
		}
	}
	
	private boolean checkBoxCollision() {
		// Checks for collisions between the main box and the other boring boxes
		for (Box b : this.boxes) {
			// (if the main box intersects with another box)
			if (this.mainBox.getX() <= b.getX()+this.boxWidth
					&& this.mainBox.getX() >= b.getX()-this.boxWidth
					&& this.mainBox.getY() <= b.getY()+this.boxWidth
					&& this.mainBox.getY() >= b.getY()-this.boxWidth) {
				// TODO: Box collision rebound logic
				return true;
			}
		}
		return false;
	}
	
	private void moveBox() {
		// Move the box according to the angle
		// and adjust the angle if the box hits a wall or another box
		// TODO: Fix box-to-box collision handling
		if (this.angle == 0) {
			if (this.mainBox.getY() >= (600-this.boxWidth))
				this.angle = 1;
			if (this.mainBox.getX() >= (800-this.boxWidth))
				this.angle = 3;
			this.mainBox.setX(this.mainBox.getX() + 1);
			this.mainBox.setY(this.mainBox.getY() + 1);
		}
		else if (this.angle == 1) {
			if (this.mainBox.getY() <= 0)
				this.angle = 0;
			if (this.mainBox.getX() >= (800-this.boxWidth))
				this.angle = 2;
			this.mainBox.setX(this.mainBox.getX() + 1);
			this.mainBox.setY(this.mainBox.getY() - 1);
		}
		else if (this.angle == 2) {
			if (this.mainBox.getY() <= 0)
				this.angle = 3;
			if (this.mainBox.getX() <= 0)
				this.angle = 1;
			this.mainBox.setX(this.mainBox.getX() - 1);
			this.mainBox.setY(this.mainBox.getY() - 1);
		}
		else {
			if (this.mainBox.getY() >= (600-this.boxWidth))
				this.angle = 2;
			if (this.mainBox.getX() <= 0)
				this.angle = 0;
			this.mainBox.setX(this.mainBox.getX() - 1);
			this.mainBox.setY(this.mainBox.getY() + 1);
		}
	}

	@Override
	public void render() {
		display();
		update();
	}

	@Override
	public void resize(int width, int height) {
		// Load the Project matrix. Next commands will be applied on that matrix.
		Gdx.gl11.glMatrixMode(GL11.GL_PROJECTION);
		Gdx.gl11.glLoadIdentity();
		
		// Set up a two-dimensional orthographic viewing region.
		Gdx.glu.gluOrtho2D(Gdx.gl10, 0, width, 0, height);

		// Set up affine transformation of x and y from world coordinates to window coordinates
		Gdx.gl11.glViewport(0, 0, width, height);
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
}
