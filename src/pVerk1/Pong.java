package pVerk1;

import java.awt.Toolkit;
import java.nio.FloatBuffer;
import java.util.Random;

import org.lwjgl.opengl.GL11;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.BufferUtils;

public class Pong implements ApplicationListener {

	// Game constants
	private float BALL_RADIUS = 10;
	private float PADDLE_WIDTH = BALL_RADIUS;
	private float PADDLE_HEIGHT = 140;
	
	// Randy, the random variable... Because you never know.
	Random randy = new Random();
	
	// All-purpose vertex buffer
	private FloatBuffer vertexBuffer;
	
	// Ball velocity vector
	private Vector2 velocity;
	
	// Our model objects
	private Vector2 hPad;
	private Vector2 cPad;
	private Vector2 ball;
	
	// The player scores
	private int hScore;
	private int cScore;
	
	// Stuff for text
	private SpriteBatch spriteBatch;
	private BitmapFont font;
	
	// Initialization function. Called in the beginning and when a player scores
	private void initialize(int p) {
		// Initialize ball position
		this.ball = new Vector2(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
		
		// Initialize the ball velocity vector, and set a random angle
		// within 45 degrees of true west (towards the human paddle)
		this.velocity = new Vector2(3f, 0);
		this.velocity.setAngle(randy.nextFloat()*90 + p*180 - 45);
	}
	
	@Override
	public void create() {
//		Gdx.graphics.setDisplayMode(
//				Toolkit.getDefaultToolkit().getScreenSize().width, 
//				Toolkit.getDefaultToolkit().getScreenSize().height,
//				true);
		
		// Run the reusable initialize function
		initialize(1);
		
		// Initialize the paddle positions
		this.hPad = new Vector2(this.PADDLE_WIDTH, Gdx.graphics.getHeight()/2 - this.PADDLE_HEIGHT/2);
		this.cPad = new Vector2(Gdx.graphics.getWidth() - this.PADDLE_WIDTH*2, Gdx.graphics.getHeight()/2 - this.PADDLE_HEIGHT/2);
		
		// Initialize the player scores
		this.hScore = this.cScore = 0;
		
		// Initialize text stuff
		this.spriteBatch = new SpriteBatch();
		this.font = new BitmapFont();
		this.font.setScale(2.5f);
		this.font.setColor(0, .5f, 1f, 1);
		
		// Enable vertex arrays
		Gdx.gl11.glEnableClientState(GL11.GL_VERTEX_ARRAY);
		
		// Specify the background color of the window
		Gdx.gl11.glClearColor(0, 0, 0, 1);
		
		// Create float buffers for the paddles and ball respectively
		// and then shoving them into the same buffer
		this.vertexBuffer = BufferUtils.newFloatBuffer(652);
		float[] floatBuf = new float[652];
		float[] tempBuf = new float[] {
				0,					0,
				0,					this.PADDLE_HEIGHT,
				this.PADDLE_WIDTH,	0,
				this.PADDLE_WIDTH,	this.PADDLE_HEIGHT};
		for (int i = 0; i < 8; i++) {
			floatBuf[i] = tempBuf[i];
		}
		
		double pi = Math.PI;
		int s = 8;
		for (double i = 0; i<2*pi; i += pi/120){
			floatBuf[s++] = (float)Math.cos(i);
			floatBuf[s++] = (float)Math.sin(i);
		}
		this.vertexBuffer.put(floatBuf);
		this.vertexBuffer.rewind();
	}
	
	@Override
	public void render() {
		display();
		update();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resize(int width, int height) {
		// Load the Project matrix. Next commands will be applied on that matrix.
		Gdx.gl11.glMatrixMode(GL11.GL_PROJECTION);
		Gdx.gl11.glLoadIdentity();
		
		// Set up a two-dimensional orthographic viewing region.
		Gdx.glu.gluOrtho2D(Gdx.gl10, 0, width, 0, height);

		// Set up affine transformation of x and y from world coordinates to window coordinates
		Gdx.gl11.glViewport(0, 0, width, height);
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}
	
	private void display() {
		// Clear the screen
		Gdx.gl11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		// Enter ModelView matrix mode
		Gdx.gl11.glMatrixMode(GL11.GL_MODELVIEW);

		// Reset the ModelView matrix
		Gdx.gl11.glLoadIdentity();
		
		Gdx.gl11.glVertexPointer(2, GL11.GL_FLOAT, 0, this.vertexBuffer);
		
		// Draw the human pad
		Gdx.gl11.glPushMatrix();
		Gdx.gl11.glTranslatef(hPad.x, hPad.y, 0);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		Gdx.gl11.glPopMatrix();
		
		// Draw the computer pad
		Gdx.gl11.glPushMatrix();
		Gdx.gl11.glTranslatef(cPad.x, cPad.y, 0);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		Gdx.gl11.glPopMatrix();
		
		// Draw the ball
		Gdx.gl11.glPushMatrix();
		Gdx.gl11.glTranslatef(ball.x, ball.y, 0);
		Gdx.gl11.glScalef(this.BALL_RADIUS, this.BALL_RADIUS, this.BALL_RADIUS);
		Gdx.gl11.glDrawArrays(GL11.GL_TRIANGLE_FAN, 8, 240);
		Gdx.gl11.glPopMatrix();
		
		// Draw the current score
		String hScoreStr = String.format("%d", hScore);
		String cScoreStr = String.format("%d", cScore);
		this.spriteBatch.begin();
		this.font.draw(
				this.spriteBatch,
				hScoreStr,
				Gdx.graphics.getWidth()/2 - font.getBounds(hScoreStr).width - 25,
				590);
		this.font.draw(
				this.spriteBatch,
				cScoreStr,
				Gdx.graphics.getWidth()/2 + 25,
				590);
		this.spriteBatch.end();
	}
	
	private void update() {
		lControl(this.hPad);
		rControl(this.cPad);
		checkForWin();
		checkForCollision();
		this.ball.add(this.velocity);
	}
	
	private void rControl(Vector2 v) {
		float t = 5;
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			if (v.y + this.PADDLE_HEIGHT + t > Gdx.graphics.getHeight()) {
				t = Gdx.graphics.getHeight() - (v.y + this.PADDLE_HEIGHT);
			}
			v.add(0, t);
		}
		if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			if (v.y - t < 0) {
				t = v.y;
			}
			v.sub(0, t);
		}
	}
	private void lControl(Vector2 v) {
		float t = 5;
		if (Gdx.input.isKeyPressed(Keys.W)) {
			if (v.y + this.PADDLE_HEIGHT + t > Gdx.graphics.getHeight()) {
				t = Gdx.graphics.getHeight() - (v.y + this.PADDLE_HEIGHT);
			}
			v.add(0, t);
		}
		if (Gdx.input.isKeyPressed(Keys.S)) {
			if (v.y - t < 0) {
				t = v.y;
			}
			v.sub(0, t);
		}
	}
	
	// This always wins (Only for demonstration purposes)
	private void automove(Vector2 v) {
		if (this.ball.y + this.PADDLE_HEIGHT/2 < Gdx.graphics.getHeight()
				&& this.ball.y - this.PADDLE_HEIGHT/2 > 0)
			v.y = this.ball.y-this.PADDLE_HEIGHT/2;
	}
	
	private void checkForWin() {
		if (this.ball.x+this.BALL_RADIUS < 0) {
			++cScore;
			initialize(1);
		}
		else if (this.ball.x-this.BALL_RADIUS > Gdx.graphics.getWidth()) {
			++hScore;
			initialize(0);
		}
	}
	
	private void checkForCollision() {
		// Check for floor/ceiling collision
		if (this.ball.y+this.BALL_RADIUS > Gdx.graphics.getHeight())
			reflect(new Vector2(0, -1));
		else if (this.ball.y-this.BALL_RADIUS < 0)
			reflect(new Vector2(0, 1));
		
		// Check for paddle collision
		// Paddles will act as if they were slightly concave
		if (this.ball.x-this.BALL_RADIUS <= this.PADDLE_WIDTH*2
				&& this.ball.x-this.BALL_RADIUS > this.PADDLE_WIDTH*2 - this.velocity.len()
				&& this.ball.y >= this.hPad.y
				&& this.ball.y <= this.hPad.y+this.PADDLE_HEIGHT) {
			float rotation = (this.ball.y - (this.hPad.y+(this.PADDLE_HEIGHT/2)))/(this.PADDLE_HEIGHT/2);
			this.velocity.setAngle(rotation * 72);
			this.velocity.mul(1.2f);
		}
		else if (this.ball.x+this.BALL_RADIUS >= Gdx.graphics.getWidth()-this.PADDLE_WIDTH*2
				&& this.ball.x+this.BALL_RADIUS < Gdx.graphics.getWidth() - this.PADDLE_WIDTH*2 + this.velocity.len()
				&& this.ball.y >= this.cPad.y
				&& this.ball.y <= this.cPad.y+this.PADDLE_HEIGHT) {
			float rotation = (this.ball.y - (this.cPad.y+(this.PADDLE_HEIGHT/2)))/(this.PADDLE_HEIGHT/2);
			this.velocity.setAngle(-1 * rotation * 72 + 180);
			this.velocity.mul(1.2f);
		}
	}
	
	private void reflect(Vector2 n) {
		// Apply the equation r = a - 2n( a dot n )
		float dot = this.velocity.tmp().dot(n);
		this.velocity.sub(n.tmp().mul(2).mul(dot));
	}

}
