package pVerk1;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;

import dVerk1.BoxWorld;

public class PongDesktopRunner {
	
	public static void main(String[] args) {
		new LwjglApplication(new Pong(), "Pong!", 800, 600, false);
	}
}
